#include <Arduino.h>
#include <Wire.h>
#include "SRF02.hpp"
#include "OLED_ARD3.hpp" //inclusion de la lib OLED_ARD2

#define REDPIN D0
#define GREENPIN D7
#define BLUEPIN D8

int sda = D2;
int scl = D1;
int addressI2C = 0x70;
int addressOled = 0x78;
SRF02 srf(addressI2C,sda,scl);
OLED_ARD3 u8g2(/* addrI2C=*/ 0x3C, /* sda=*/ D5, /* scl=*/ D6); //création d'un objet de la lib

int distance = 0;

void setup() {
  //initialisation de l'afficheur OLED
  u8g2.begin(); 
  //affichage sur les 4 premières lignes
  u8g2.SetLine("Lib OLED_ARD3", 0);
  u8g2.SetLine("HAMIDOU", 1);
  u8g2.SetLine("Alix", 2);
  u8g2.SetLine("ISIndustrie", 3);

  pinMode(REDPIN, OUTPUT);    //On definie le pin de la led rouge en sortie
  pinMode(GREENPIN, OUTPUT);    //On definie le pin de la led rouge en sortie
  pinMode(BLUEPIN, OUTPUT);    //On definie le pin de la led rouge en sortie

  Serial.begin(9600);
}

void loop() {
  distance = srf.LireDistance();
  String msg = (String)distance + " cm";
  Serial.println(msg); 
  //affichage sur la 5ème ligne
  u8g2.SetLine(String(distance), 4);

  if(distance<50){
    digitalWrite(REDPIN, HIGH);
    digitalWrite(GREENPIN, LOW);
    digitalWrite(BLUEPIN, LOW);
  }
  if(distance>50 && distance<100){
    digitalWrite(REDPIN, LOW);
    digitalWrite(GREENPIN, LOW);
    digitalWrite(BLUEPIN, HIGH);
  }
  if(distance>100){
    digitalWrite(REDPIN, LOW);
    digitalWrite(GREENPIN, HIGH);
    digitalWrite(BLUEPIN, LOW);
  }

  delay(250);
}