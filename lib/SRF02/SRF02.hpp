#pragma once
#include <Arduino.h>
#include <Wire.h>

class SRF02 {
    private:
        int reading;
        int sda;
        int scl;
        int addressI2C;
    public:
        SRF02(int _address, int _sda, int _scl);
        ~SRF02();
        int LireDistance();
};