#include "SRF02.hpp"

SRF02::SRF02(int _address, int _sda, int _scl) {
    reading = 0;
    sda = _sda;
    scl = _scl;
    addressI2C = _address;
    Wire.begin(sda,scl);
}

SRF02::~SRF02() {

}

int SRF02::LireDistance(){
    Wire.beginTransmission(addressI2C); // transmit to device #112 (0x70)
    Wire.write(byte(0x00)); // sets register pointer to the command register (0x00)
    Wire.write(byte(0x51)); // command sensor to measure in "centimeters" (0x51)
    Wire.endTransmission(); // stop transmitting
    delay(70); // datasheet suggests at least 65 milliseconds
    Wire.beginTransmission(addressI2C); // transmit to device #112
    Wire.write(byte(0x02)); // sets register pointer to echo #1 register (0x02)
    Wire.endTransmission(); // stop transmitting
    Wire.requestFrom(addressI2C, 2); // request 2 bytes from slave device #112
    // step 5: receive reading from sensor
    if (2 <= Wire.available()) // if two bytes were received
    {
        reading = Wire.read(); // receive high byte (overwrites previous reading)
        reading = reading << 8; // shift high byte to be high 8 bits
        reading |= Wire.read(); // receive low byte as lower 8 bits
    }
    delay(250);
    return reading;
}