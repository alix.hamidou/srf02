# OLED_ARD3 : classe pour afficher sur l'écran OLED SSD1306 128x64

> * Auteur : Gwénaël LAURENT
> * Date : 16/01/2021
> * PlatformIO : Core 5.0.4 Home 3.3.1

- [OLED_ARD3 : classe pour afficher sur l'écran OLED SSD1306 128x64](#oled_ard3--classe-pour-afficher-sur-lécran-oled-ssd1306-128x64)
  - [1. Description](#1-description)
  - [2. Dépendances](#2-dépendances)
  - [3. Intégration dans VScode / PlatformIO](#3-intégration-dans-vscode--platformio)
  - [4. Ajout de fonctionnalités](#4-ajout-de-fonctionnalités)

## 1. Description
Classe d'affichage sur l'écran OLED sd1306 128x64 connecté en I2C. 
* Elle permet de contrôler l'affichage sur 5 lignes (Affichage avec buffer)
* Adaptation pour carte ESP32 version WEMOS lolin 32 + OLED (addr = 0x3C, SDA=GPIO5, SCL=GPIO4) 
* avec le framework Arduino

## 2. Dépendances
* Nécessite l'installation de la library [U8g2 by oliver (olikraus@gmail.com)](https://github.com/olikraus/u8g2)
* La bibliothèque OLED_ARD2 est une classe dérivée de la classe U8G2.
  * modification de l'initialisation pour correspondre à la carte lolin 32 + OLED
  * ajout de méthodes pour gérer le buffer d'affichage sur 5 lignes, etc ...


## 3. Intégration dans VScode / PlatformIO
1. Menu PlatformIO > Library : Installation de la lib ```U8g2 by oliver (olikraus@gmail.com)```
2. Copie du dossier de la lib ```OLED_ARD3``` dans le dosiier ```/lib``` du projet
3. utilisation dans ```/src/main.cpp```

**Exemple 1 : **
* Affichage fixe sur 5 lignes

```cpp
#include <Arduino.h>
#include "OLED_ARD3.hpp" //inclusion de la lib OLED_ARD2

OLED_ARD3 u8g2(/* addrI2C=*/ 0x3C, /* sda=*/ 5, /* scl=*/ 4); //création d'un objet de la lib

void setup(void)
{
  //initialisation de l'afficheur OLED
  u8g2.begin();
  //affichage sur les 4 premières lignes
  u8g2.SetLine("Lib OLED_ARD2", 0);
  u8g2.SetLine("LAURENT", 1);
  u8g2.SetLine("Gwénaël", 2);
  u8g2.SetLine("BTS SNIR - Armentières", 3);
}

void loop(void)
{
  //affichage sur la 5ème ligne
  u8g2.SetLine(String(millis()), 4);
  delay(1000);
}
```

**Exemple 2 : **
* Affichage de l'adresse MAC
* Affichage d'un logo
* Affichage fixe sur 5 lignes
* Scroll des 4 dernières lignes

```cpp
#include <Arduino.h>
#include "OLED_ARD3.hpp" //inclusion de la lib OLED_ARD3
#include "WiFi.h"        //pour afficher adresse MAC

OLED_ARD3 u8g2(/* addrI2C=*/ 0x3C, /* sda=*/ 5, /* scl=*/ 4); //création d'un objet de la lib

void setup(void) {
  //initialisation de l'afficheur OLED
  u8g2.begin();
  //initialisation du WiFi en station
  WiFi.mode(WIFI_MODE_STA);
  //affichage sur les 5 lignes
  u8g2.SetLine(WiFi.macAddress(), 0);
  u8g2.AfficherLogo();
  delay(2000);
  u8g2.SetLine("LAURENT", 1);
  u8g2.SetLine("Gwénaël", 2);
  u8g2.SetLine("BTS SNIR Armentières", 3);
  u8g2.SetLine(String(millis()), 4);
  delay(2000);
}

void loop(void) {
  //scroll sur les 4 dernières lignes
  u8g2.AddLineScroll(String(millis()));
  delay(1000);
}
```

## 4. Ajout de fonctionnalités
Il suffit d'ajouter des méthodes à la classe OLED_ARD3 et d'utiliser les méthodes de la classe U8G2. Comme la classe OLED_ARD3 dérive de la classe U8G2, toutes les méthodes de la classe U8GE sont accessibles directement.

Documentation sur la lib U8G2
[https://github.com/olikraus/u8g2/wiki/u8g2reference](https://github.com/olikraus/u8g2/wiki/u8g2reference)

> **ATTENTION** : Utiliser les exemples "C++/Arduino"
