/**
 * Author : GwénaëlLAURENT 
 * Repository : https://gitlab.com/gwenael.laurent/public/-/tree/master/platformio
 * Date : 16/01/2021
 * PlatformIO : Core 5.0.4 Home 3.3.1
 * Licence CC-BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)
 * */

#ifndef OLED_ARD3_H
#define OLED_ARD3_H

#include <Arduino.h>
#include <Wire.h>
#include <U8g2lib.h>
#include "logo.h"

#define NBLIGNES 5

/**
	 * @brief  Classe d'affichage sur l'écran 
   * OLED sd1306 128x64 connecté en I2C (addr=0x3C, SDA=GPIO5, SCL=GPIO4) 
   * Adaptation pour fonctionner sur ESP32 
   * WEMOS lolin 32 + OLED 
   * avec le framework Arduino
   * Affichage avec buffer.
   * 
   * Dépendance : U8g2 by oliver (olikraus@gmail.com)
   * 
	 */
class OLED_ARD3 : public U8G2
{

private:

    /**
   *  @brief Tableau contenant le texte de chacune des lignes
   */
    String arrayLigne[NBLIGNES];

    /**
   *  @brief état de couleur de fond
   */
    bool whiteBackground = false;

    /**
	 * @brief   Affiche les textes contenus dans arrayLigne[]
	 * @return  Rien
	 */
    void show5lines();

public:
    /**
	 * @brief   Constructeur : Initialise l'accès de l'écran OLED
	 * @param   _address Adresse I2C de l'afficheur (0x3C)
	 * @param   _sda broche SDA (5)
	 * @param   _scl Broche SCL (4)
	 */
    OLED_ARD3(uint8_t _address, uint8_t _sda, uint8_t _scl);

    /**
	 * @brief   Modifie le texte de la première ligne
	 * @param   prmText  Le nouveau texte
	 * @return  Rien
	 */
    void SetFirstLine(String prmText);

    /**
	 * @brief   Modifie le texte d'une ligne
	 * @param   prmText  Le nouveau texte
	 * @param   prmNumLigne  Le numéro de la ligne à modifier (0 à NBLIGNES)
	 * @return  Rien
	 */
    void SetLine(String prmText, int prmNumLigne = 0);

    /**
	 * @brief   Ajoute une nouvelle ligne en bas de l'écran. 
     * Les 4 anciennes lignes "scrollent" vers le haut
	 * @param   prmText  Le nouveau texte à afficher
	 * @return  Rien
	 */
    void AddLineScroll(String prmText);

    /**
	 * @brief   Ajoute une nouvelle ligne en bas de l'écran. 
     * Les anciennes lignes "scrollent" vers le haut
	 * @param   prmText  Le nouveau texte à afficher
	 * @param   prmNumLigneDebut  Le scroll des lignes s'effectue entre la ligne 
     * prmNumLigneDebut et NBLIGNES (par défaut, les 4 dernières lignes de l'écran "scrollent")
	 * @return  Rien
	 */
    void AddLineScrollFrom(String prmText, int prmNumLigneDebut = 1);

    /**
	 * @brief   Efface l'écran et le buffer d'affichage
	 * @return  Rien
	 */
    void effacerEcran();

    /**
	 * @brief   Modifie la couleur du fond
   *  @param prmColor : 0 pour texte Blanc/fond Noir
   *         prmColor : 1 pour texte Noir/fond Blanc 
	 * @return  Rien
	 */
    void SetWhiteBackGround(bool prmColor = false);

    /**
	 * @brief   Affiche le logo contenu dans logo.h
	 * @return  Rien
	 */
    void AfficherLogo();

    void test();
    
    /**
	 * @brief   Affichage d'un texte en gros au centre de l'écran
     * Utilisation : ex pour la pression et la température
     * u8g2.Afficher2Valeurs("Pression atmos.", (String)pression + " hPa", "Température", (String)temperature + " " + "\xb0" + "C");
	 * @param   prmMessage  Texte à afficher
	 * @return  Rien
	 */
    void afficherAuCentre(String prmMessage);

    /**
	 * @brief   Affichage de 2 valeurs avec 2 textes
     * Utilisation : ex pour la pression et la température
     * u8g2.Afficher2Valeurs("Pression atmos.", (String)pression + " hPa", "Température", (String)temperature + " " + "\xb0" + "C");
	 * @param   txt1  Texte accompagnant la 1ère valeur
     * @param   val1  1ère valeur
     * @param   txt2  Texte accompagnant la 2ème valeur
     * @param   val2  2ème valeur
	 * @return  Rien
	 */
    void Afficher2Valeurs(String txt1 = "", String val1 = "", String txt2 = "", String val2 = "");
};

#endif